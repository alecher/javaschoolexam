package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     *                  decimal mark, parentheses, operations signs '+', '-', '*',
     *                  '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is
     *         invalid
     */

    public String evaluate(String statement) {

        // https://en.wikipedia.org/wiki/Shunting-yard_algorithm
        try {
            double result = calculate(PRN(statement));
            return fmt(result);
        } catch (Exception e) {
            return null;
        }

    }

    private static double calculate(String postfix) throws Exception {
        double x = 0;
        double y = 0;

        StringTokenizer str = new StringTokenizer(postfix);
        Deque<Double> stack = new ArrayDeque<Double>();
        String token;

        while (str.hasMoreTokens()) {
            token = str.nextToken().trim();
            if (1 == token.length() && ops.containsKey(token.charAt(0))) {
                y = stack.pop();
                x = stack.pop();

                switch (token.charAt(0)) {
                case '+':
                    x += y;
                    break;
                case '-':
                    x -= y;
                    break;
                case '*':
                    x *= y;
                    break;
                case '/':
                    if (y == 0)
                        throw new Exception("Cannot divide by 0");
                    x /= y;
                    break;

                }

                stack.push(x);
            } else {
                x = Double.parseDouble(token);
                stack.push(x);
            }

        }

        return stack.pop();
    }

    private static String PRN(String infix) throws Exception {

        StringBuilder stack = new StringBuilder("");
        StringBuilder result = new StringBuilder("");

        char current;
        char last;

        for (int i = 0; i < infix.length(); i++) {
            current = infix.charAt(i);

            if (Character.isDigit(current) || current == '.') {
                result.append(current);
            } else if (ops.containsKey(current)) {

                while (stack.length() > 0) {
                    last = stack.substring(stack.length() - 1).charAt(0);
                    if (ops.containsKey(last) && ops.get(current).precedence <= ops.get(last).precedence) {
                        result.append(" " + last + " ");
                        stack.setLength(stack.length() - 1);
                    } else {
                        result.append(" ");
                        break;
                    }
                }

                result.append(" ");
                stack.append(current);
            } else if ('(' == current) {
                stack.append(current);
            } else if (')' == current) {
                last = stack.substring(stack.length() - 1).charAt(0);

                while ('(' != last) {
                    if (stack.length() < 1) {
                        throw new Exception("Parentheses mismatch");
                    }
                    result.append(" " + last);
                    stack.setLength(stack.length() - 1);
                    last = stack.substring(stack.length() - 1).charAt(0);
                }

                stack.setLength(stack.length() - 1);
            } else {
                throw new Exception("Invalid character " + current);
            }
        }

        while (stack.length() > 0) {
            result.append(" ").append(stack.substring(stack.length() - 1));
            stack.setLength(stack.length() - 1);
        }

        return result.toString();
    }

    private enum Operator {
        ADD(0), SUBTRACT(0), MULTIPLY(1), DIVIDE(1);

        final int precedence;

        Operator(int p) {
            precedence = p;
        }
    }

    private static Map<Character, Operator> ops = new HashMap<Character, Operator>() {
        {
            put('+', Operator.ADD);
            put('-', Operator.SUBTRACT);
            put('*', Operator.MULTIPLY);
            put('/', Operator.DIVIDE);
        }
    };

    // https://stackoverflow.com/a/14126736/7603105
    private static String fmt(double d) {

        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }

}

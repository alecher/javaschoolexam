package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import com.tsystems.javaschool.tasks.pyramid.CannotBuildPyramidException;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and
     * maximum at the bottom, from left to right). All vacant positions in the array
     * are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build
     *                with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (!isInputValid(inputNumbers))
            throw new CannotBuildPyramidException();

        int listSize = inputNumbers.size();
        int rowCount = 0;
        int totalSize = rowCount;

        while (totalSize < listSize) {
            rowCount++;
            totalSize += rowCount;
        }

        Collections.sort(inputNumbers);

        int lastRowSize = 2 * rowCount - 1;
        int[][] pyramid = new int[rowCount][lastRowSize];
        int listIndex = 0;

        for (int i = 0; i < rowCount; i++) {
            int offset = rowCount - 1 - i;
            for (int j = 0; j < lastRowSize; j++) {
                if (j >= offset && j <= lastRowSize - offset && Math.abs(i - j) % 2 != rowCount % 2) {
                    pyramid[i][j] = inputNumbers.get(listIndex++);
                } else {
                    pyramid[i][j] = 0;
                }
            }
        }

        return pyramid;
    }

    private static boolean isInputValid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null))
            return false;

        // https://math.stackexchange.com/a/466653
        int listSize = inputNumbers.size();
        double sr = Math.sqrt(listSize * 8 + 1);
        if ((sr - Math.floor(sr)) != 0)
            return false;

        return true;
    }

}
